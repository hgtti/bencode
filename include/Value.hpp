/*
 * Value.hpp
 *
 *  Created on: Sep 16, 2014
 *      Author: Tyler
 */

#ifndef VALUE_HPP_
#define VALUE_HPP_

#include <string>
#include <vector>
#include <map>

#include "Exception.hpp"

namespace Bencoder {

class Value {

public:

	enum Type {
		INT,
		STRING,
		LIST,
		HASH,
		NUL
	};

	~Value();

	/**
	 * Default value is null
	 */
	Value(){
		value = NULL;
	}

	Value(int);
	/**
	 * Use this version of the string constructor to allow null characters.
	 */
	Value(const char*, unsigned int length);
	Value(const std::string);
	Value(const std::vector<Value>&);
	Value(const std::map<std::string, Value>&);
	Value(const Value&);

	class TypeException : Exception {
	public:
		TypeException() : Exception("Invalid Type"){};
	};

	class TypeStateException : Exception {
	public:
		TypeStateException() : Exception("Illegal State Exception! Invalid type."){};
	};

	int getInt() const;
	std::vector<Value>* getList() const;
	std::string* getString() const;
	std::map<std::string, Value>* getHash() const;
	Type getType() const;

	Value& set(int);
	Value& set(const char*, unsigned int length);
	Value& set(const std::string&);
	Value& set(const std::vector<Value>&);
	Value& set(const std::map<std::string, Value>&);

	Value& operator+(int);
	Value& operator-(int);

	Value& operator=(const Value& v);

	inline Value& operator=(int val){
		set(val);
		return *this;
	}

	inline Value& operator=(const std::string& s){
		set(s);
		return *this;
	}

	inline Value& operator=(const std::vector<Value>& val){
		set(val);
		return *this;
	}

	inline Value& operator=(const std::map<std::string, Value>& val){
		set(val);
		return *this;
	}

	inline Value& operator[](unsigned int x) const {
		return getList()->at(x);
	}

	inline Value& operator[](const std::string& x) const {
		return getHash()->at(x);
	}

	bool operator==(const Value&) const;
	bool operator==(const Type) const;

	std::string toString() const;

private:

	void Delete();

protected:

	//Internal type tracking.
	Type type = NUL; //Default is null.

	void* value;

};

}


#endif /* VALUE_HPP_ */
