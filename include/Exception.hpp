/*
 * Exception.hpp
 *
 *  Created on: Sep 17, 2014
 *      Author: Tyler
 */

#ifndef EXCEPTION_HPP_
#define EXCEPTION_HPP_

#include <string>

namespace Bencoder {

class Exception {

private:

	const std::string why;

public:

	Exception(std::string why) : why(why) {}

	const std::string what() const {
		return why;
	}

};

}

#endif /* EXCEPTION_HPP_ */
