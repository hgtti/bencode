/*
 * FileReader.hpp
 *
 *  Created on: Sep 16, 2014
 *      Author: Tyler
 */

#ifndef BENCODER_HPP_
#define BENCODER_HPP_

#include <istream>
#include <ostream>

#include "Value.hpp"

namespace Bencoder {

class Encoder {

protected:

	Value root;

private:

	Value readInt(std::istream&);
	Value readList(std::istream&);
	Value readHash(std::istream&);
	Value readString(std::istream&, unsigned int length);
	Value rread(std::istream&);

	void rwrite(const Value& v, std::ostream&) const;

public:

	Encoder() : root() {};

	void read(std::istream&);
	void write(std::ostream&) const;

	Value& getData(){
		return root;
	}

	void setData(Value& v){
		root = v;
	}

	std::istream& operator<<(std::istream& in){
		read(in);
		return in;
	}
	std::ostream& operator>>(std::ostream& out) const{
		write(out);
		return out;
	}

};

}

#endif /* FILEREADER_HPP_ */
