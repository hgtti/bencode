#!/bin/sh

#echo "Building..."
#make -C ..

echo "Running tests on the files."
../build/out/Default/test files/small.torrent test1.out
../build/out/Default/test files/large.torrent test2.out

#echo "Diffing files:"
#diff files/small.torrent test1.out
#diff files/large.torrent test2.out

#echo "Cleaning output."
#rm *.out

