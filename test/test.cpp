
#include <iostream>
#include <fstream>

#include "Bencoder.hpp"

using namespace std;
using namespace Bencoder;

int main(int args, char* argc[]){

	if (args != 3){
		cout << "Usage: [file in] [file out]" << endl;
	} else {

		ifstream in(argc[1]);

		if (!in.good()){
			cout << "File was not present or had no data." << endl;
			return 0;
		}

		Encoder e;

		e << in;

		in.close();

		ofstream out(argc[2]);

		if (!out.good()){
			cout << "Could not write to output file." << endl;
			return 0;
		}

		e >> out;

	}

}
