/*
 * FileReader.cpp
 *
 *  Created on: Sep 16, 2014
 *      Author: Tyler
 */

#include <vector>
#include <map>
#include <string>

#include "Bencoder.hpp"
#include "Value.hpp"

using namespace Bencoder;
using namespace std;

Value Encoder::readInt(istream& ss){

	ss.seekg(1, ios_base::cur); //Skip i

	int val;
	ss >> val;

	ss.seekg(1, ios_base::cur); //Skip e

	return Value(val);

}

Value Encoder::readList(istream& ss){

	ss.seekg(1, ios_base::cur); //Skip l

	vector<Value> list = vector<Value>();

	while (ss.peek() != 'e'){
		list.push_back(rread(ss));
	}

	ss.seekg(1, ios_base::cur); //Skip e

	return Value(list);

}

Value Encoder::readHash(istream& ss){

	ss.seekg(1, ios_base::cur); //Skip d

	map<string, Value> hash = map<string, Value>();

	unsigned int length;

	char* buffer;

	while (ss.peek() != 'e'){

		ss >> length;

		ss.seekg(1, ios_base::cur); //Skip :

		buffer = new char[length + 1]();

		ss.read(buffer, length);

		string s = string(buffer);

		delete[] buffer;

		Value v = rread(ss);

		hash.insert({s, v});

	}

	ss.seekg(1, ios_base::cur); //Skip e

	return Value(hash);

}

Value Encoder::readString(istream& ss, unsigned int length){

	ss.seekg(1, ios_base::cur); //Skip :

	char* buffer = new char[length + 1](); //Zero'd character array.
	ss.read(buffer, length);

	Value v(buffer, length);

	delete[] buffer;

	return v;

}

Value Encoder::rread(istream& ss){

	char delimiter = ss.peek();

	switch (delimiter){
	case 'i': //Integer
		return readInt(ss);
	case 'l': //List
		return readList(ss);
	case 'd': //Hash
		return readHash(ss);
	default: //Try reading a length for string. If its not then the file is wrong.
		unsigned int length;
		ss >> length;
		return readString(ss, length);
	}

}

void Encoder::read(istream& in){

	try{
		root = rread(in);
	} catch (...){
		throw Exception("File incorrectly formated or corrupted.");
	}

}

void Encoder::rwrite(const Value& v, ostream& out) const {

	switch (v.getType()){
	case Value::INT: out << "i" << v.getInt() << "e"; break;
	case Value::STRING:
	{
		string s = *(v.getString());
		out << s.length() << ":" << s;
	}
		break;
	case Value::LIST:
	{
		out << "l";
		for (Value& v2 : *(v.getList())){
			rwrite(v2, out);
		}
		out << "e";
	}
		break;
	case Value::HASH:
	{
		out << "d";
		for (const pair<string, Value>& p : *(v.getHash())){
			out << p.first.length() << ":" << p.first;
			rwrite(p.second, out);
		}
		out << "e";
	}
		break;
	case Value::NUL: break;
	default: throw Value::TypeStateException();
	}

}

void Encoder::write(ostream& out) const {

	if (root.getType() != Value::NUL){
		rwrite(root, out);
	}

}

