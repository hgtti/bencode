
#include "Value.hpp"

using namespace Bencoder;
using namespace std;

Value::~Value(){
	Delete();
}

void Value::Delete(){

	switch (type){
	case Type::INT: delete ((int*) value); break;
	case Type::STRING: delete ((string*) value); break;
	case Type::LIST: delete ((vector<Value>*) value); break;
	case Type::HASH: delete ((map<string, Value>*) value); break;
	case Type::NUL: break; //Do nothing. Nothing to delete.
	default: throw TypeStateException();
	}

}

Value::Value(int x){
	value = new int(x);
	type = INT;
}

Value::Value(const char* s, unsigned int length){
	value = new string(s, length);
	type = STRING;
}

Value::Value(const string s){
	value = new string(s);
	type = STRING;
}

Value::Value(const vector<Value>& v){
	value = new vector<Value>(v);
	type = LIST;
}

Value::Value(const map<string, Value>& m){
	value = new map<string, Value>(m);
	type = HASH;
}

Value::Value(const Value& v){

	type = v.type;

	switch (type){
	case Type::INT: value = new int(*((int*) v.value)); break;
	case Type::STRING: value = new string(*((string*) v.value)); break;
	case Type::LIST: value = new vector<Value>(*((vector<Value>*) v.value)); break;
	case Type::HASH: value = new map<string, Value>(*((map<string, Value>*) v.value)); break;
	case Type::NUL: value = NULL; break;
	default: throw TypeStateException();
	}

}

int Value::getInt() const{
	if (type == INT){
		return *((int*)value);
	} else {
		throw TypeException();
	}
}

string* Value::getString() const {
	if (type == STRING){
		return (string*) value;
	} else {
		throw TypeException();
	}
}

vector<Value>* Value::getList() const {
	if (type == LIST){
		return (vector<Value>*) value;
	} else {
		throw TypeException();
	}
}

map<string, Value>* Value::getHash() const {
	if (type == HASH){
		return (map<string, Value>*) value;
	} else {
		throw TypeException();
	}
}

Value::Type Value::getType() const {
	return type;
}

Value& Value::set(int x){
	Delete();
	value = new int(x);
	type = INT;
	return *this;
}

Value& Value::set(const char* s, unsigned int length){
	Delete();
	value = new string(s, length);
	type = STRING;
	return *this;
}

Value& Value::set(const string& s){
	Delete();
	value = new string(s);
	type = STRING;
	return *this;
}

Value& Value::set(const vector<Value>& v){
	Delete();
	value = new vector<Value>(v);
	type = LIST;
	return *this;
}

Value& Value::set(const map<string, Value>& m){
	Delete();
	value = new map<string, Value>(m);
	type = HASH;
	return *this;
}

bool Value::operator==(const Type t) const {
	return (type == t);
}

bool Value::operator==(const Value& v) const {
	if (type != v.type) return false;
	switch (type){
	case Type::INT: return *((int*) value) == *((int*) v.value);
	case Type::STRING: return *((string*) value) == *((string*) v.value);
	case Type::LIST: return *((vector<Value>*) value) == *((vector<Value>*) v.value);
	case Type::HASH: return *((map<string, Value>*) value) == *((map<string, Value>*) v.value);
	case Type::NUL: return true;
	default: throw TypeStateException();
	}
}

Value& Value::operator=(const Value& v) {

	Delete();

	type = v.type;

	switch (type){
	case Type::INT: value = new int(*((int*) v.value)); break;
	case Type::STRING: value = new string(*((string*) v.value)); break;
	case Type::LIST: value = new vector<Value>(*((vector<Value>*) v.value)); break;
	case Type::HASH: value = new map<string, Value>(*((map<string, Value>*) v.value)); break;
	case Type::NUL: value = NULL; break;
	default: throw TypeStateException();
	}

	return *this;

}
