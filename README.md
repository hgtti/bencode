Bencoder
========

Implementation of [Bencoding](http://en.wikipedia.org/wiki/Bencode)

API
---

__Value:__

Dynamic type, can be an int, byte string, list, or associative hash.

Protocols like torrents can expect values of certain names and locations to be certain types and directly access
them, but other programs that don't know explicit formats or expected formats should check types.

* `Value()` - 
* `Value(int)`
* `Value(char*, unsigned int length)`
* `Value(string)`
* `Value(vector<Value>)`
* `Value(map<string, Value>)`
* `getInt()` - Returns integer form of data.
* `getString()` - Returns string form of data. (`std::string`)
* `getList()` - Returns `std::vector` of values
* `getHash()` - Returns `std::map` of values.
* `set(int)` - Sets value to int
* `set(char*, unsigned int length)` - Sets value to string (Use for binary)
* `set(string)` - Sets value to string
* `set(vector<Value>)` - Sets value to list
* `set(map<string,Value>)` - Sets the value to a sorted map.
* Operators
    - `operator+` Add (works only for ints)
    - `operator-` Subtract (works only for ints)
    - `operator=` Copy
    - `operator==` Equals
    - `operator[int]` Get index of array (Only for lists)
    - `operator[string]` Get element of hash

__Bencoder:__

Reads and writes files from Value trees.

* `write(ostream)` - Writes root tree to file.
* `read(istream)` - Reads file to root tree.
* `getRoot()` - Gets the root node of the tree.
* `setRoot(Value)` - Sets the root to a new Value.
* Operators
    - `operator<<(istream)` - read from istream (will read until root is closed)
    - `operator>>(ostream)` - write to ostream (writes whole root node tree to ostream)

License
-------

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.

