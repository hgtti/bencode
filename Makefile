V=1

all: Bencoder

Bencoder: ./build/Makefile
	@echo "Running build"
	make -C ./build

./build/Makefile: ./deps/gyp/gyp build.gyp
	@echo "Generating build files."
	./deps/gyp/gyp build.gyp --depth=. -f make --generator-output=./build

./deps/gyp/gyp:
	@echo "You need gyp to generate the files, downloading it."
	git clone --depth 1 https://chromium.googlesource.com/external/gyp.git ./deps/gyp

clean:
	rm -rf ./build

