{
	"targets": [
		{
			"target_name": "test",
			"type": "executable",
			"sources": [
				"test/test.cpp"
			],
			"dependencies": [
				"Bencoder"
			],
			"cflags": [
				"-O3",
				"-Wall",
				"-pg"
			]
		},
		{
			"target_name": "Bencoder",
			"product_name": "Bencoder",
			"type": "static_library",
			"sources": [
				"src/Value.cpp",
				"src/Bencoder.cpp"
			],
			"include_dirs": [
				"include"
			],
			"cflags": [
				"-std=c++11",
				"-O3",
				"-Wall",
				"-Wextra",
				"-pedantic"
			],
			"direct_dependent_settings": {
				"include_dirs": [
					"include"
				],
				"cflags": [
					"-std=c++11"
				]
			}
		}
	]
}

